# m165 - Script

The content of this repository is complementary to the [main repository](https://gitlab.com/ch-tbz-it/Stud/m165/m165_scripts). You need the content to automate the installation of the linux/ubuntu server. 

The content of the folder "scripts", will be executed automatically, when you follow all the documented steps.


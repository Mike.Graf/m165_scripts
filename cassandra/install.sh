#!/bin/bash
sudo apt-get update -y

# =============== Cassandra
sudo apt update
#sudo apt install openjdk-16-jdk
#sudo apt install openjdk-8-jdk
#sudo update-alternatives --config java #needed, if there were several different java installations
wget -q -O - https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add --
sudo sh -c 'echo "deb http://www.apache.org/dist/cassandra/debian 311x main" \
> /etc/apt/sources.list.d/cassandra.sources.list'
sudo apt update
sudo apt install cassandra  -y

sudo sed -i 's/start_rpc: false/start_rpc: true/g' /etc/cassandra/cassandra.yaml
sudo sed -i 's/rpc_address: localhost/rpc_address: 0.0.0.0/g' /etc/cassandra/cassandra.yaml
sudo sed -i 's/# broadcast_rpc_address: 1.2.3.4/broadcast_rpc_address: localhost/g' /etc/cassandra/cassandra.yaml

sudo service cassandra restart

# cqlsh # this would start the console
# nodetool status # check status of server
#!/bin/bash
sudo apt-get update -y

# =============== Neo4j
curl -fsSL https://debian.neo4j.com/neotechnology.gpg.key |sudo gpg --dearmor -o /usr/share/keyrings/neo4j.gpg
echo "deb [signed-by=/usr/share/keyrings/neo4j.gpg] https://debian.neo4j.com stable 4.1" | sudo tee -a /etc/apt/sources.list.d/neo4j.list
sudo apt update
sudo apt install neo4j -y
sudo systemctl enable neo4j.service
sudo systemctl start neo4j.service

sudo sed -i 's/#dbms.default_listen_address/dbms.default_listen_address/g' /etc/neo4j/neo4j.conf

sudo systemctl restart neo4j.service
#!/bin/bash

sudo mongosh <<EOS
use admin;
	db.createUser(
	  {
		user: "$1",
		pwd: "$2", // or cleartext password
		roles: [
		  { role: "userAdminAnyDatabase", db: "admin" },
		  { role: "readWriteAnyDatabase", db: "admin" }
		]
	  }
	);
EOS
sudo mongosh --authenticationDatabase "admin" -u "$1" -p "$2" <<EOS
use school;
db.createCollection("Module");
db.createCollection("Teacher");
db.createCollection("Class");
var class01 = new ObjectId();
var class02 = new ObjectId();
var stud01 = new ObjectId();
var stud02 = new ObjectId();
var stud03 = new ObjectId();
var stud04 = new ObjectId();
var stud05 = new ObjectId();
var stud06 = new ObjectId();
var stud07 = new ObjectId();
var stud08 = new ObjectId();
var stud09 = new ObjectId();
var stud10 = new ObjectId();
db.Class.insertOne({_id:class01, Name:'AP22a', YearStart:2022, Students:[
	{_id:stud01, Firstname:'Huberta', Lastname: 'Jurk'},
	{_id:stud02, Firstname:'Phillip', Lastname: 'Teich'},
	{_id:stud03, Firstname:'Torben', Lastname: 'Hansel'},
	{_id:stud04, Firstname:'Ekkehart', Lastname: 'Eckmann'},
	{_id:stud05, Firstname:'Laurentius', Lastname: 'Elze'}
]});
db.Class.insertOne({_id:class02, Name:'AP21a', YearStart:2021, Students:[
	{_id:stud06, Firstname:'Celia', Lastname: 'Ludolph'},
	{_id:stud07, Firstname:'Dustin', Lastname: 'Guggemos'},
	{_id:stud08, Firstname:'Ferdinand', Lastname: 'Gutierrez'},
	{_id:stud09, Firstname:'Wilderich', Lastname: 'Wallat'},
	{_id:stud09, Firstname:'Valerian', Lastname: 'Steding'}
]});
var mod01 = new ObjectId();
var mod02 = new ObjectId();
var mod03 = new ObjectId();
var mod04 = new ObjectId();
var mod05 = new ObjectId();
var mod06 = new ObjectId();
db.Module.insertOne({_id:mod01, Name:'m162', Description:'Daten analysieren und modellieren', Grades:[
	{StudentId:stud01, Grade:5.6},
	{StudentId:stud02, Grade:6},
	{StudentId:stud03, Grade:4.3},
	{StudentId:stud04, Grade:5.2},
	{StudentId:stud05, Grade:4.1},
	{StudentId:stud06, Grade:5.2},
	{StudentId:stud07, Grade:6},
	{StudentId:stud08, Grade:5.2},
	{StudentId:stud09, Grade:4.1},
	{StudentId:stud10, Grade:4.1}
]});
db.Module.insertOne({_id:mod02, Name:'m164', Description:'Datenbanken erstellen und Daten einfügen', Grades:[
	{StudentId:stud01, Grade:5},
	{StudentId:stud02, Grade:5.2},
	{StudentId:stud03, Grade:5.3},
	{StudentId:stud04, Grade:4.1},
	{StudentId:stud05, Grade:4.8},
	{StudentId:stud06, Grade:3},
	{StudentId:stud07, Grade:4.5},
	{StudentId:stud08, Grade:6},
	{StudentId:stud09, Grade:4.1},
	{StudentId:stud10, Grade:4.6}
]});
db.Module.insertOne({_id:mod03, Name:'m165', Description:'NoSQL-Datenbanken einsetzen', Grades:[
	{StudentId:stud01, Grade:5.8},
	{StudentId:stud02, Grade:4.8},
	{StudentId:stud03, Grade:5.1},
	{StudentId:stud04, Grade:4.5},
	{StudentId:stud05, Grade:4.1},
	{StudentId:stud06, Grade:3.5},
	{StudentId:stud07, Grade:4},
	{StudentId:stud08, Grade:5.4},
	{StudentId:stud09, Grade:3.8},
	{StudentId:stud10, Grade:4.1}
]});
db.Module.insertOne({_id:mod04, Name:'m319', Description:'Applikationen entwerfen und implementieren', Grades:[
	{StudentId:stud01, Grade:5.1},
	{StudentId:stud02, Grade:4.4},
	{StudentId:stud03, Grade:4.7},
	{StudentId:stud04, Grade:5.1},
	{StudentId:stud05, Grade:4.4},
	{StudentId:stud06, Grade:4.1},
	{StudentId:stud07, Grade:5.2},
	{StudentId:stud08, Grade:5.8},
	{StudentId:stud09, Grade:4.2},
	{StudentId:stud10, Grade:3.3}
]});
db.Module.insertOne({_id:mod05, Name:'m320', Description:'Objektorientiert Programmieren', Grades:[
	{StudentId:stud01, Grade:5.5},
	{StudentId:stud02, Grade:5.2},
	{StudentId:stud03, Grade:5.3},
	{StudentId:stud04, Grade:4.9},
	{StudentId:stud05, Grade:4.6},
	{StudentId:stud06, Grade:3.8},
	{StudentId:stud07, Grade:4.2},
	{StudentId:stud08, Grade:4.3},
	{StudentId:stud09, Grade:4.9},
	{StudentId:stud10, Grade:4.4}
]});
db.Module.insertOne({_id:mod06, Name:'m322', Description:'Benutzerschnittstellen entwerfen und implementieren', Grades:[
	{StudentId:stud01, Grade:5.8},
	{StudentId:stud02, Grade:5.4},
	{StudentId:stud03, Grade:5.6},
	{StudentId:stud04, Grade:5.1},
	{StudentId:stud05, Grade:4.9},
	{StudentId:stud06, Grade:4.5},
	{StudentId:stud07, Grade:4.8},
	{StudentId:stud08, Grade:4.4},
	{StudentId:stud09, Grade:5.1},
	{StudentId:stud10, Grade:4.8}
]});
var teach01 = new ObjectId();
var teach02 = new ObjectId();
var teach03 = new ObjectId();
var teach04 = new ObjectId();
db.Teacher.insertOne({_id: teach01, Firstname:'Amalia', Lastname: 'Zech', Teach:[
	{Module: mod01, Class: class01},
	{Module: mod05, Class: class01},
	{Module: mod03, Class: class02},
	{Module: mod05, Class: class02},
	{Module: mod06, Class: class02}
]});
db.Teacher.insertOne({_id: teach02, Firstname:'Valentine', Lastname: 'Pollak', Teach:[
	{Module: mod02, Class: class01},
	{Module: mod06, Class: class01},
	{Module: mod04, Class: class02}
]});
db.Teacher.insertOne({_id: teach03, Firstname:'Margareta', Lastname: 'Kraska', Teach:[
	{Module: mod03, Class: class01},
	{Module: mod01, Class: class02}
]});
db.Teacher.insertOne({_id: teach04, Firstname:'Reinbert', Lastname: 'Bretthauer', Teach:[
	{Module: mod04, Class: class01},
	{Module: mod02, Class: class02}
]});
EOS

# create content of database
# sudo mongosh  < ~/m165_scripts/mongodb/mongodb.txt

use intro

drop table if exists TeachingArrangement;
drop table if exists Grade;
drop table if exists ItModule;
drop table if exists StudentInClass;
drop table if exists SchoolClass;
drop table if exists Teacher;
drop table if exists Student;

create table SchoolClass(
    Id int auto_increment,
    Name varchar(255) not null,
    YearStart int not null,
	ClassSpeakerId int,
	ClassTeacherId int,
    primary key(Id)
);

create table Student(
    Id int auto_increment,
    Firstname varchar(255) not null,
    Lastname varchar(255) not null,
	ClassId int not null,
    primary key(Id),
	foreign key(ClassId)
        references SchoolClass(Id)
);

create table Teacher(
    Id int auto_increment,
    Firstname varchar(255) not null,
    Lastname varchar(255) not null,
    primary key(Id)
);

ALTER TABLE SchoolClass
ADD FOREIGN KEY (ClassSpeakerId) REFERENCES Student(Id);

ALTER TABLE SchoolClass
ADD FOREIGN KEY (ClassTeacherId) REFERENCES Teacher(Id);

create table StudentInClass(
    StudentId int,
    ClassId int,
    primary key(StudentId, ClassId),
	foreign key(StudentId)
        references Student(Id),
	foreign key(ClassId)
        references SchoolClass(Id)
);

create table ItModule(
    Id int auto_increment,
    Name varchar(255) not null,
    Description varchar(255) not null,
    primary key(Id)
);

create table Grade(
    StudentId int,
    ModuleId int,
	Grade float,
    primary key(StudentId, ModuleId),
	foreign key(StudentId)
        references Student(Id),
	foreign key(ModuleId)
        references ItModule(Id)
);

create table TeachingArrangement(
    ClassId int,
    ModuleId int,
	TeacherId int,
    primary key(ClassId, ModuleId, TeacherId),
	foreign key(ClassId)
        references SchoolClass(Id),
	foreign key(ModuleId)
        references ItModule(Id),
	foreign key(TeacherId)
        references Teacher(Id)
);